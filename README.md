# SSegning auth

A new Flutter package project.

## Dependencies

- [OAuth2 client](https://raw.githubusercontent.com/teranetsrl/oauth2_client/master/README.md)
- [Secure storage](https://raw.githubusercontent.com/mogol/flutter_secure_storage/develop/README.md)

## Config

## Android ##

If Android is one of your targets, you must first set the *minSdkVersion* in the *build.gradle* file:
```
defaultConfig {
   ...
   minSdkVersion 18
   ...
```

Again on Android, if your application uses the Authorization Code flow, you first need to modify the *AndroidManifest.xml* file adding the intent filter needed to open the browser window for the authorization workflow.
The library relies on the flutter_web_auth package to allow the Authorization Code flow.

AndroidManifest.xml

```xml
<activity android:name="com.linusu.flutter_web_auth.CallbackActivity" >
	<intent-filter android:label="flutter_web_auth">
		<action android:name="android.intent.action.VIEW" />
		<category android:name="android.intent.category.DEFAULT" />
		<category android:name="android.intent.category.BROWSABLE" />
		<data android:scheme="my.test.app" />
	</intent-filter>
</activity>
```

## iOS ##
On iOS you need to set the *platform* in the *ios/Podfile* file:
```
platform :ios, '11.0'
```

