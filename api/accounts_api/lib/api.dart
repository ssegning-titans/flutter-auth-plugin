//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/accounts_api.dart';
part 'api/credentials_api.dart';

part 'model/account.dart';
part 'model/account_address.dart';
part 'model/account_all_of.dart';
part 'model/account_gender.dart';
part 'model/account_status.dart';
part 'model/account_type.dart';
part 'model/base_account.dart';
part 'model/change_personal_data_request.dart';
part 'model/count_response.dart';
part 'model/create_account_input.dart';
part 'model/create_credential_input.dart';
part 'model/create_credential_response.dart';
part 'model/credential_type.dart';
part 'model/disable_credential_response.dart';
part 'model/has_account_password.dart';
part 'model/lat_lng.dart';
part 'model/recover_password_response.dart';
part 'model/validate_password_input.dart';
part 'model/validate_password_response.dart';
part 'model/validate_recovery_code_input.dart';
part 'model/validate_recovery_code_response.dart';
part 'model/validate_recovery_token_input.dart';


const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

ApiClient defaultApiClient = ApiClient();
