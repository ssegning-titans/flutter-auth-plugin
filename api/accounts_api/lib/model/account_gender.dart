//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class AccountGender {
  /// Instantiate a new enum with the provided [value].
  const AccountGender._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  String toString() => value;

  String toJson() => value;

  static const male = AccountGender._(r'male');
  static const female = AccountGender._(r'female');
  static const other = AccountGender._(r'other');

  /// List of all possible values in this [enum][AccountGender].
  static const values = <AccountGender>[
    male,
    female,
    other,
  ];

  static AccountGender? fromJson(dynamic value) => AccountGenderTypeTransformer().decode(value);

  static List<AccountGender>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <AccountGender>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AccountGender.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }
}

/// Transformation class that can [encode] an instance of [AccountGender] to String,
/// and [decode] dynamic data back to [AccountGender].
class AccountGenderTypeTransformer {
  factory AccountGenderTypeTransformer() => _instance ??= const AccountGenderTypeTransformer._();

  const AccountGenderTypeTransformer._();

  String encode(AccountGender data) => data.value;

  /// Decodes a [dynamic value][data] to a AccountGender.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  AccountGender? decode(dynamic data, {bool allowNull = true}) {
    if (data != null) {
      switch (data.toString()) {
        case r'male': return AccountGender.male;
        case r'female': return AccountGender.female;
        case r'other': return AccountGender.other;
        default:
          if (!allowNull) {
            throw ArgumentError('Unknown enum value to decode: $data');
          }
      }
    }
    return null;
  }

  /// Singleton [AccountGenderTypeTransformer] instance.
  static AccountGenderTypeTransformer? _instance;
}

