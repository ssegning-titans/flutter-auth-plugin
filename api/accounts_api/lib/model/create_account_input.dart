//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CreateAccountInput {
  /// Returns a new [CreateAccountInput] instance.
  CreateAccountInput({
    this.firstName,
    this.lastName,
    this.title,
    required this.email,
    this.phoneNumber,
    this.gender,
    this.locale,
    this.bio,
    this.avatarUrl,
    this.accountType,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? firstName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? lastName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? title;

  String email;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? phoneNumber;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountGender? gender;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? locale;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? bio;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? avatarUrl;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountType? accountType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CreateAccountInput &&
     other.firstName == firstName &&
     other.lastName == lastName &&
     other.title == title &&
     other.email == email &&
     other.phoneNumber == phoneNumber &&
     other.gender == gender &&
     other.locale == locale &&
     other.bio == bio &&
     other.avatarUrl == avatarUrl &&
     other.accountType == accountType;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (firstName == null ? 0 : firstName!.hashCode) +
    (lastName == null ? 0 : lastName!.hashCode) +
    (title == null ? 0 : title!.hashCode) +
    (email.hashCode) +
    (phoneNumber == null ? 0 : phoneNumber!.hashCode) +
    (gender == null ? 0 : gender!.hashCode) +
    (locale == null ? 0 : locale!.hashCode) +
    (bio == null ? 0 : bio!.hashCode) +
    (avatarUrl == null ? 0 : avatarUrl!.hashCode) +
    (accountType == null ? 0 : accountType!.hashCode);

  @override
  String toString() => 'CreateAccountInput[firstName=$firstName, lastName=$lastName, title=$title, email=$email, phoneNumber=$phoneNumber, gender=$gender, locale=$locale, bio=$bio, avatarUrl=$avatarUrl, accountType=$accountType]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (firstName != null) {
      _json[r'firstName'] = firstName;
    }
    if (lastName != null) {
      _json[r'lastName'] = lastName;
    }
    if (title != null) {
      _json[r'title'] = title;
    }
      _json[r'email'] = email;
    if (phoneNumber != null) {
      _json[r'phoneNumber'] = phoneNumber;
    }
    if (gender != null) {
      _json[r'gender'] = gender;
    }
    if (locale != null) {
      _json[r'locale'] = locale;
    }
    if (bio != null) {
      _json[r'bio'] = bio;
    }
    if (avatarUrl != null) {
      _json[r'avatarUrl'] = avatarUrl;
    }
    if (accountType != null) {
      _json[r'accountType'] = accountType;
    }
    return _json;
  }

  /// Returns a new [CreateAccountInput] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static CreateAccountInput? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "CreateAccountInput[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "CreateAccountInput[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return CreateAccountInput(
        firstName: mapValueOfType<String>(json, r'firstName'),
        lastName: mapValueOfType<String>(json, r'lastName'),
        title: mapValueOfType<String>(json, r'title'),
        email: mapValueOfType<String>(json, r'email')!,
        phoneNumber: mapValueOfType<String>(json, r'phoneNumber'),
        gender: AccountGender.fromJson(json[r'gender']),
        locale: mapValueOfType<String>(json, r'locale'),
        bio: mapValueOfType<String>(json, r'bio'),
        avatarUrl: mapValueOfType<String>(json, r'avatarUrl'),
        accountType: AccountType.fromJson(json[r'accountType']),
      );
    }
    return null;
  }

  static List<CreateAccountInput>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <CreateAccountInput>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = CreateAccountInput.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, CreateAccountInput> mapFromJson(dynamic json) {
    final map = <String, CreateAccountInput>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CreateAccountInput.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of CreateAccountInput-objects as value to a dart map
  static Map<String, List<CreateAccountInput>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<CreateAccountInput>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = CreateAccountInput.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'email',
  };
}

