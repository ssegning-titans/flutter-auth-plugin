//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ValidatePasswordInput {
  /// Returns a new [ValidatePasswordInput] instance.
  ValidatePasswordInput({
    required this.challenge,
  });

  String challenge;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ValidatePasswordInput &&
     other.challenge == challenge;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (challenge.hashCode);

  @override
  String toString() => 'ValidatePasswordInput[challenge=$challenge]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
      _json[r'challenge'] = challenge;
    return _json;
  }

  /// Returns a new [ValidatePasswordInput] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ValidatePasswordInput? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "ValidatePasswordInput[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "ValidatePasswordInput[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ValidatePasswordInput(
        challenge: mapValueOfType<String>(json, r'challenge')!,
      );
    }
    return null;
  }

  static List<ValidatePasswordInput>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <ValidatePasswordInput>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ValidatePasswordInput.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ValidatePasswordInput> mapFromJson(dynamic json) {
    final map = <String, ValidatePasswordInput>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ValidatePasswordInput.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ValidatePasswordInput-objects as value to a dart map
  static Map<String, List<ValidatePasswordInput>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<ValidatePasswordInput>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ValidatePasswordInput.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'challenge',
  };
}

