//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class RecoverPasswordResponse {
  /// Returns a new [RecoverPasswordResponse] instance.
  RecoverPasswordResponse({
    required this.code,
    required this.status,
  });

  String code;

  String status;

  @override
  bool operator ==(Object other) => identical(this, other) || other is RecoverPasswordResponse &&
     other.code == code &&
     other.status == status;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (code.hashCode) +
    (status.hashCode);

  @override
  String toString() => 'RecoverPasswordResponse[code=$code, status=$status]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
      _json[r'code'] = code;
      _json[r'status'] = status;
    return _json;
  }

  /// Returns a new [RecoverPasswordResponse] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static RecoverPasswordResponse? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "RecoverPasswordResponse[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "RecoverPasswordResponse[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return RecoverPasswordResponse(
        code: mapValueOfType<String>(json, r'code')!,
        status: mapValueOfType<String>(json, r'status')!,
      );
    }
    return null;
  }

  static List<RecoverPasswordResponse>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <RecoverPasswordResponse>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = RecoverPasswordResponse.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, RecoverPasswordResponse> mapFromJson(dynamic json) {
    final map = <String, RecoverPasswordResponse>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = RecoverPasswordResponse.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of RecoverPasswordResponse-objects as value to a dart map
  static Map<String, List<RecoverPasswordResponse>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<RecoverPasswordResponse>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = RecoverPasswordResponse.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'code',
    'status',
  };
}

