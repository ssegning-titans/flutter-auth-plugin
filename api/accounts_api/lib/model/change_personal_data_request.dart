//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ChangePersonalDataRequest {
  /// Returns a new [ChangePersonalDataRequest] instance.
  ChangePersonalDataRequest({
    this.email,
    this.firstName,
    this.lastName,
    this.bio,
    this.phoneNumber,
    this.avatarUrl,
    this.phoneNumberValid,
    this.locale,
    this.gender,
    this.accountType,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? email;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? firstName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? lastName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? bio;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? phoneNumber;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? avatarUrl;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? phoneNumberValid;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? locale;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountGender? gender;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountType? accountType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ChangePersonalDataRequest &&
     other.email == email &&
     other.firstName == firstName &&
     other.lastName == lastName &&
     other.bio == bio &&
     other.phoneNumber == phoneNumber &&
     other.avatarUrl == avatarUrl &&
     other.phoneNumberValid == phoneNumberValid &&
     other.locale == locale &&
     other.gender == gender &&
     other.accountType == accountType;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (email == null ? 0 : email!.hashCode) +
    (firstName == null ? 0 : firstName!.hashCode) +
    (lastName == null ? 0 : lastName!.hashCode) +
    (bio == null ? 0 : bio!.hashCode) +
    (phoneNumber == null ? 0 : phoneNumber!.hashCode) +
    (avatarUrl == null ? 0 : avatarUrl!.hashCode) +
    (phoneNumberValid == null ? 0 : phoneNumberValid!.hashCode) +
    (locale == null ? 0 : locale!.hashCode) +
    (gender == null ? 0 : gender!.hashCode) +
    (accountType == null ? 0 : accountType!.hashCode);

  @override
  String toString() => 'ChangePersonalDataRequest[email=$email, firstName=$firstName, lastName=$lastName, bio=$bio, phoneNumber=$phoneNumber, avatarUrl=$avatarUrl, phoneNumberValid=$phoneNumberValid, locale=$locale, gender=$gender, accountType=$accountType]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (email != null) {
      _json[r'email'] = email;
    }
    if (firstName != null) {
      _json[r'firstName'] = firstName;
    }
    if (lastName != null) {
      _json[r'lastName'] = lastName;
    }
    if (bio != null) {
      _json[r'bio'] = bio;
    }
    if (phoneNumber != null) {
      _json[r'phoneNumber'] = phoneNumber;
    }
    if (avatarUrl != null) {
      _json[r'avatarUrl'] = avatarUrl;
    }
    if (phoneNumberValid != null) {
      _json[r'phoneNumberValid'] = phoneNumberValid;
    }
    if (locale != null) {
      _json[r'locale'] = locale;
    }
    if (gender != null) {
      _json[r'gender'] = gender;
    }
    if (accountType != null) {
      _json[r'accountType'] = accountType;
    }
    return _json;
  }

  /// Returns a new [ChangePersonalDataRequest] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static ChangePersonalDataRequest? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "ChangePersonalDataRequest[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "ChangePersonalDataRequest[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return ChangePersonalDataRequest(
        email: mapValueOfType<String>(json, r'email'),
        firstName: mapValueOfType<String>(json, r'firstName'),
        lastName: mapValueOfType<String>(json, r'lastName'),
        bio: mapValueOfType<String>(json, r'bio'),
        phoneNumber: mapValueOfType<String>(json, r'phoneNumber'),
        avatarUrl: mapValueOfType<String>(json, r'avatarUrl'),
        phoneNumberValid: mapValueOfType<bool>(json, r'phoneNumberValid'),
        locale: mapValueOfType<String>(json, r'locale'),
        gender: AccountGender.fromJson(json[r'gender']),
        accountType: AccountType.fromJson(json[r'accountType']),
      );
    }
    return null;
  }

  static List<ChangePersonalDataRequest>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <ChangePersonalDataRequest>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = ChangePersonalDataRequest.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, ChangePersonalDataRequest> mapFromJson(dynamic json) {
    final map = <String, ChangePersonalDataRequest>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ChangePersonalDataRequest.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of ChangePersonalDataRequest-objects as value to a dart map
  static Map<String, List<ChangePersonalDataRequest>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<ChangePersonalDataRequest>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = ChangePersonalDataRequest.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
  };
}

