//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Account {
  /// Returns a new [Account] instance.
  Account({
    required this.createdAt,
    required this.updatedAt,
    required this.id,
    this.title,
    this.firstName,
    this.lastName,
    this.locale,
    this.avatarUrl,
    required this.email,
    this.emailVerified,
    this.gender,
    required this.status,
    this.phoneNumber,
    this.phoneNumberVerified,
    this.bio,
    this.accountType,
    this.addresses = const [],
  });

  int createdAt;

  int updatedAt;

  String id;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? title;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? firstName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? lastName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? locale;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? avatarUrl;

  String email;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? emailVerified;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountGender? gender;

  AccountStatus status;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? phoneNumber;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  bool? phoneNumberVerified;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? bio;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  AccountType? accountType;

  List<AccountAddress> addresses;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Account &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.id == id &&
     other.title == title &&
     other.firstName == firstName &&
     other.lastName == lastName &&
     other.locale == locale &&
     other.avatarUrl == avatarUrl &&
     other.email == email &&
     other.emailVerified == emailVerified &&
     other.gender == gender &&
     other.status == status &&
     other.phoneNumber == phoneNumber &&
     other.phoneNumberVerified == phoneNumberVerified &&
     other.bio == bio &&
     other.accountType == accountType &&
     other.addresses == addresses;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (createdAt.hashCode) +
    (updatedAt.hashCode) +
    (id.hashCode) +
    (title == null ? 0 : title!.hashCode) +
    (firstName == null ? 0 : firstName!.hashCode) +
    (lastName == null ? 0 : lastName!.hashCode) +
    (locale == null ? 0 : locale!.hashCode) +
    (avatarUrl == null ? 0 : avatarUrl!.hashCode) +
    (email.hashCode) +
    (emailVerified == null ? 0 : emailVerified!.hashCode) +
    (gender == null ? 0 : gender!.hashCode) +
    (status.hashCode) +
    (phoneNumber == null ? 0 : phoneNumber!.hashCode) +
    (phoneNumberVerified == null ? 0 : phoneNumberVerified!.hashCode) +
    (bio == null ? 0 : bio!.hashCode) +
    (accountType == null ? 0 : accountType!.hashCode) +
    (addresses.hashCode);

  @override
  String toString() => 'Account[createdAt=$createdAt, updatedAt=$updatedAt, id=$id, title=$title, firstName=$firstName, lastName=$lastName, locale=$locale, avatarUrl=$avatarUrl, email=$email, emailVerified=$emailVerified, gender=$gender, status=$status, phoneNumber=$phoneNumber, phoneNumberVerified=$phoneNumberVerified, bio=$bio, accountType=$accountType, addresses=$addresses]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
      _json[r'createdAt'] = createdAt;
      _json[r'updatedAt'] = updatedAt;
      _json[r'id'] = id;
    if (title != null) {
      _json[r'title'] = title;
    }
    if (firstName != null) {
      _json[r'firstName'] = firstName;
    }
    if (lastName != null) {
      _json[r'lastName'] = lastName;
    }
    if (locale != null) {
      _json[r'locale'] = locale;
    }
    if (avatarUrl != null) {
      _json[r'avatarUrl'] = avatarUrl;
    }
      _json[r'email'] = email;
    if (emailVerified != null) {
      _json[r'emailVerified'] = emailVerified;
    }
    if (gender != null) {
      _json[r'gender'] = gender;
    }
      _json[r'status'] = status;
    if (phoneNumber != null) {
      _json[r'phoneNumber'] = phoneNumber;
    }
    if (phoneNumberVerified != null) {
      _json[r'phoneNumberVerified'] = phoneNumberVerified;
    }
    if (bio != null) {
      _json[r'bio'] = bio;
    }
    if (accountType != null) {
      _json[r'accountType'] = accountType;
    }
      _json[r'addresses'] = addresses;
    return _json;
  }

  /// Returns a new [Account] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static Account? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "Account[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "Account[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return Account(
        createdAt: mapValueOfType<int>(json, r'createdAt')!,
        updatedAt: mapValueOfType<int>(json, r'updatedAt')!,
        id: mapValueOfType<String>(json, r'id')!,
        title: mapValueOfType<String>(json, r'title'),
        firstName: mapValueOfType<String>(json, r'firstName'),
        lastName: mapValueOfType<String>(json, r'lastName'),
        locale: mapValueOfType<String>(json, r'locale'),
        avatarUrl: mapValueOfType<String>(json, r'avatarUrl'),
        email: mapValueOfType<String>(json, r'email')!,
        emailVerified: mapValueOfType<bool>(json, r'emailVerified'),
        gender: AccountGender.fromJson(json[r'gender']),
        status: AccountStatus.fromJson(json[r'status'])!,
        phoneNumber: mapValueOfType<String>(json, r'phoneNumber'),
        phoneNumberVerified: mapValueOfType<bool>(json, r'phoneNumberVerified'),
        bio: mapValueOfType<String>(json, r'bio'),
        accountType: AccountType.fromJson(json[r'accountType']),
        addresses: AccountAddress.listFromJson(json[r'addresses']) ?? const [],
      );
    }
    return null;
  }

  static List<Account>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <Account>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = Account.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, Account> mapFromJson(dynamic json) {
    final map = <String, Account>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = Account.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of Account-objects as value to a dart map
  static Map<String, List<Account>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<Account>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = Account.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
    'createdAt',
    'updatedAt',
    'id',
    'email',
    'status',
  };
}

