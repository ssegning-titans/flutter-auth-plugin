//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AccountAddress {
  /// Returns a new [AccountAddress] instance.
  AccountAddress({
    this.id,
    this.creationDate,
    this.phoneNumber,
    this.accountId,
    this.region,
    this.formattedName,
    this.street,
    this.houseNumber,
    this.city,
    this.zip,
    this.country,
    this.location,
  });

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? id;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  int? creationDate;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? phoneNumber;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? accountId;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? region;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? formattedName;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? street;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? houseNumber;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? city;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? zip;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  String? country;

  ///
  /// Please note: This property should have been non-nullable! Since the specification file
  /// does not include a default value (using the "default:" property), however, the generated
  /// source code must fall back to having a nullable type.
  /// Consider adding a "default:" property in the specification file to hide this note.
  ///
  LatLng? location;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AccountAddress &&
     other.id == id &&
     other.creationDate == creationDate &&
     other.phoneNumber == phoneNumber &&
     other.accountId == accountId &&
     other.region == region &&
     other.formattedName == formattedName &&
     other.street == street &&
     other.houseNumber == houseNumber &&
     other.city == city &&
     other.zip == zip &&
     other.country == country &&
     other.location == location;

  @override
  int get hashCode =>
    // ignore: unnecessary_parenthesis
    (id == null ? 0 : id!.hashCode) +
    (creationDate == null ? 0 : creationDate!.hashCode) +
    (phoneNumber == null ? 0 : phoneNumber!.hashCode) +
    (accountId == null ? 0 : accountId!.hashCode) +
    (region == null ? 0 : region!.hashCode) +
    (formattedName == null ? 0 : formattedName!.hashCode) +
    (street == null ? 0 : street!.hashCode) +
    (houseNumber == null ? 0 : houseNumber!.hashCode) +
    (city == null ? 0 : city!.hashCode) +
    (zip == null ? 0 : zip!.hashCode) +
    (country == null ? 0 : country!.hashCode) +
    (location == null ? 0 : location!.hashCode);

  @override
  String toString() => 'AccountAddress[id=$id, creationDate=$creationDate, phoneNumber=$phoneNumber, accountId=$accountId, region=$region, formattedName=$formattedName, street=$street, houseNumber=$houseNumber, city=$city, zip=$zip, country=$country, location=$location]';

  Map<String, dynamic> toJson() {
    final _json = <String, dynamic>{};
    if (id != null) {
      _json[r'id'] = id;
    }
    if (creationDate != null) {
      _json[r'creationDate'] = creationDate;
    }
    if (phoneNumber != null) {
      _json[r'phoneNumber'] = phoneNumber;
    }
    if (accountId != null) {
      _json[r'accountId'] = accountId;
    }
    if (region != null) {
      _json[r'region'] = region;
    }
    if (formattedName != null) {
      _json[r'formattedName'] = formattedName;
    }
    if (street != null) {
      _json[r'street'] = street;
    }
    if (houseNumber != null) {
      _json[r'houseNumber'] = houseNumber;
    }
    if (city != null) {
      _json[r'city'] = city;
    }
    if (zip != null) {
      _json[r'zip'] = zip;
    }
    if (country != null) {
      _json[r'country'] = country;
    }
    if (location != null) {
      _json[r'location'] = location;
    }
    return _json;
  }

  /// Returns a new [AccountAddress] instance and imports its values from
  /// [value] if it's a [Map], null otherwise.
  // ignore: prefer_constructors_over_static_methods
  static AccountAddress? fromJson(dynamic value) {
    if (value is Map) {
      final json = value.cast<String, dynamic>();

      // Ensure that the map contains the required keys.
      // Note 1: the values aren't checked for validity beyond being non-null.
      // Note 2: this code is stripped in release mode!
      assert(() {
        requiredKeys.forEach((key) {
          assert(json.containsKey(key), 'Required key "AccountAddress[$key]" is missing from JSON.');
          assert(json[key] != null, 'Required key "AccountAddress[$key]" has a null value in JSON.');
        });
        return true;
      }());

      return AccountAddress(
        id: mapValueOfType<String>(json, r'id'),
        creationDate: mapValueOfType<int>(json, r'creationDate'),
        phoneNumber: mapValueOfType<String>(json, r'phoneNumber'),
        accountId: mapValueOfType<String>(json, r'accountId'),
        region: mapValueOfType<String>(json, r'region'),
        formattedName: mapValueOfType<String>(json, r'formattedName'),
        street: mapValueOfType<String>(json, r'street'),
        houseNumber: mapValueOfType<String>(json, r'houseNumber'),
        city: mapValueOfType<String>(json, r'city'),
        zip: mapValueOfType<String>(json, r'zip'),
        country: mapValueOfType<String>(json, r'country'),
        location: LatLng.fromJson(json[r'location']),
      );
    }
    return null;
  }

  static List<AccountAddress>? listFromJson(dynamic json, {bool growable = false,}) {
    final result = <AccountAddress>[];
    if (json is List && json.isNotEmpty) {
      for (final row in json) {
        final value = AccountAddress.fromJson(row);
        if (value != null) {
          result.add(value);
        }
      }
    }
    return result.toList(growable: growable);
  }

  static Map<String, AccountAddress> mapFromJson(dynamic json) {
    final map = <String, AccountAddress>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AccountAddress.fromJson(entry.value);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  // maps a json object with a list of AccountAddress-objects as value to a dart map
  static Map<String, List<AccountAddress>> mapListFromJson(dynamic json, {bool growable = false,}) {
    final map = <String, List<AccountAddress>>{};
    if (json is Map && json.isNotEmpty) {
      json = json.cast<String, dynamic>(); // ignore: parameter_assignments
      for (final entry in json.entries) {
        final value = AccountAddress.listFromJson(entry.value, growable: growable,);
        if (value != null) {
          map[entry.key] = value;
        }
      }
    }
    return map;
  }

  /// The list of required keys that must be present in a JSON.
  static const requiredKeys = <String>{
  };
}

