//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class AccountsApi {
  AccountsApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Get accounts by params
  ///
  /// Get `Accounts` by params. Empty `Map` means no search param, which also means to count all 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [int] size (required):
  ///
  /// * [int] offset (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<Response> countAccountsByParamsWithHttpInfo(int size, int offset, Map<String, String> requestBody,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/count/map';

    // ignore: prefer_final_locals
    Object? postBody = requestBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_queryParams('', 'size', size));
      queryParams.addAll(_queryParams('', 'offset', offset));

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// Get accounts by params
  ///
  /// Get `Accounts` by params. Empty `Map` means no search param, which also means to count all 
  ///
  /// Parameters:
  ///
  /// * [int] size (required):
  ///
  /// * [int] offset (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<CountResponse?> countAccountsByParams(int size, int offset, Map<String, String> requestBody,) async {
    final response = await countAccountsByParamsWithHttpInfo(size, offset, requestBody,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'CountResponse',) as CountResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [CreateAccountInput] createAccountInput (required):
  Future<Response> createAccountWithHttpInfo(CreateAccountInput createAccountInput,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts';

    // ignore: prefer_final_locals
    Object? postBody = createAccountInput;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [CreateAccountInput] createAccountInput (required):
  Future<Account?> createAccount(CreateAccountInput createAccountInput,) async {
    final response = await createAccountWithHttpInfo(createAccountInput,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'Account',) as Account;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [int] size (required):
  ///
  /// * [int] offset (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<Response> findAccountByAllWithHttpInfo(int size, int offset, Map<String, String> requestBody,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/find/map';

    // ignore: prefer_final_locals
    Object? postBody = requestBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_queryParams('', 'size', size));
      queryParams.addAll(_queryParams('', 'offset', offset));

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [int] size (required):
  ///
  /// * [int] offset (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<List<Account>?> findAccountByAll(int size, int offset, Map<String, String> requestBody,) async {
    final response = await findAccountByAllWithHttpInfo(size, offset, requestBody,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      final responseBody = await _decodeBodyBytes(response);
      return (await apiClient.deserializeAsync(responseBody, 'List<Account>') as List)
        .cast<Account>()
        .toList();

    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  Future<Response> getAccountByIdentifierWithHttpInfo(String id,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}'
      .replaceAll('{id}', id);

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  Future<Account?> getAccountByIdentifier(String id,) async {
    final response = await getAccountByIdentifierWithHttpInfo(id,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'Account',) as Account;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [ChangePersonalDataRequest] changePersonalDataRequest (required):
  Future<Response> updatePersonalDataWithHttpInfo(String id, ChangePersonalDataRequest changePersonalDataRequest,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/personal-data'
      .replaceAll('{id}', id);

    // ignore: prefer_final_locals
    Object? postBody = changePersonalDataRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'PUT',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [ChangePersonalDataRequest] changePersonalDataRequest (required):
  Future<void> updatePersonalData(String id, ChangePersonalDataRequest changePersonalDataRequest,) async {
    final response = await updatePersonalDataWithHttpInfo(id, changePersonalDataRequest,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [String] token (required):
  Future<Response> validateEmailWithHttpInfo(String id, String token,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/email/verify'
      .replaceAll('{id}', id);

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_queryParams('', 'token', token));

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [String] token (required):
  Future<void> validateEmail(String id, String token,) async {
    final response = await validateEmailWithHttpInfo(id, token,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
  }
}
