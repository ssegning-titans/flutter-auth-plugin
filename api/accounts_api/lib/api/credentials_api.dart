//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class CredentialsApi {
  CredentialsApi([ApiClient? apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [CreateCredentialInput] createCredentialInput (required):
  Future<Response> createCredentialWithHttpInfo(String id, CredentialType type, CreateCredentialInput createCredentialInput,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody = createCredentialInput;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [CreateCredentialInput] createCredentialInput (required):
  Future<CreateCredentialResponse?> createCredential(String id, CredentialType type, CreateCredentialInput createCredentialInput,) async {
    final response = await createCredentialWithHttpInfo(id, type, createCredentialInput,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'CreateCredentialResponse',) as CreateCredentialResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<Response> disableCredentialWithHttpInfo(String id, CredentialType type, Map<String, String> requestBody,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}/disable'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody = requestBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'PUT',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [Map<String, String>] requestBody (required):
  Future<DisableCredentialResponse?> disableCredential(String id, CredentialType type, Map<String, String> requestBody,) async {
    final response = await disableCredentialWithHttpInfo(id, type, requestBody,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'DisableCredentialResponse',) as DisableCredentialResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  Future<Response> hasCredentialWithHttpInfo(String id, CredentialType type,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  Future<HasAccountPassword?> hasCredential(String id, CredentialType type,) async {
    final response = await hasCredentialWithHttpInfo(id, type,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'HasAccountPassword',) as HasAccountPassword;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  Future<Response> recoverPasswordWithHttpInfo(String id, CredentialType type,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}/recover'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>[];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  Future<RecoverPasswordResponse?> recoverPassword(String id, CredentialType type,) async {
    final response = await recoverPasswordWithHttpInfo(id, type,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'RecoverPasswordResponse',) as RecoverPasswordResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidatePasswordInput] validatePasswordInput (required):
  Future<Response> validateCredentialWithHttpInfo(String id, CredentialType type, ValidatePasswordInput validatePasswordInput,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}/verify'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody = validatePasswordInput;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidatePasswordInput] validatePasswordInput (required):
  Future<ValidatePasswordResponse?> validateCredential(String id, CredentialType type, ValidatePasswordInput validatePasswordInput,) async {
    final response = await validateCredentialWithHttpInfo(id, type, validatePasswordInput,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'ValidatePasswordResponse',) as ValidatePasswordResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidateRecoveryCodeInput] validateRecoveryCodeInput (required):
  Future<Response> validateRecoveryPasswordWithHttpInfo(String id, CredentialType type, ValidateRecoveryCodeInput validateRecoveryCodeInput,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}/validate'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody = validateRecoveryCodeInput;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidateRecoveryCodeInput] validateRecoveryCodeInput (required):
  Future<ValidateRecoveryCodeResponse?> validateRecoveryPassword(String id, CredentialType type, ValidateRecoveryCodeInput validateRecoveryCodeInput,) async {
    final response = await validateRecoveryPasswordWithHttpInfo(id, type, validateRecoveryCodeInput,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'ValidateRecoveryCodeResponse',) as ValidateRecoveryCodeResponse;
    
    }
    return null;
  }

  /// 
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidateRecoveryTokenInput] validateRecoveryTokenInput (required):
  Future<Response> validateRecoveryTokenPasswordWithHttpInfo(String id, CredentialType type, ValidateRecoveryTokenInput validateRecoveryTokenInput,) async {
    // ignore: prefer_const_declarations
    final path = r'/v1/auth/accounts/{id}/credentials/{type}/validate-token'
      .replaceAll('{id}', id)
      .replaceAll('{type}', type.toString());

    // ignore: prefer_final_locals
    Object? postBody = validateRecoveryTokenInput;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    const contentTypes = <String>['application/json'];


    return apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      contentTypes.isEmpty ? null : contentTypes.first,
    );
  }

  /// 
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///
  /// * [CredentialType] type (required):
  ///
  /// * [ValidateRecoveryTokenInput] validateRecoveryTokenInput (required):
  Future<ValidateRecoveryCodeResponse?> validateRecoveryTokenPassword(String id, CredentialType type, ValidateRecoveryTokenInput validateRecoveryTokenInput,) async {
    final response = await validateRecoveryTokenPasswordWithHttpInfo(id, type, validateRecoveryTokenInput,);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, await _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body.isNotEmpty && response.statusCode != HttpStatus.noContent) {
      return await apiClient.deserializeAsync(await _decodeBodyBytes(response), 'ValidateRecoveryCodeResponse',) as ValidateRecoveryCodeResponse;
    
    }
    return null;
  }
}
