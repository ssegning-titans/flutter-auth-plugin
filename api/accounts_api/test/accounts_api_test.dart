//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:accounts_api/api.dart';
import 'package:test/test.dart';


/// tests for AccountsApi
void main() {
  // final instance = AccountsApi();

  group('tests for AccountsApi', () {
    // Get accounts by params
    //
    // Get `Accounts` by params. Empty `Map` means no search param, which also means to count all 
    //
    //Future<CountResponse> countAccountsByParams(int size, int offset, Map<String, String> requestBody) async
    test('test countAccountsByParams', () async {
      // TODO
    });

    // 
    //
    //Future<Account> createAccount(CreateAccountInput createAccountInput) async
    test('test createAccount', () async {
      // TODO
    });

    // 
    //
    //Future<List<Account>> findAccountByAll(int size, int offset, Map<String, String> requestBody) async
    test('test findAccountByAll', () async {
      // TODO
    });

    // 
    //
    //Future<Account> getAccountByIdentifier(String id) async
    test('test getAccountByIdentifier', () async {
      // TODO
    });

    // 
    //
    //Future updatePersonalData(String id, ChangePersonalDataRequest changePersonalDataRequest) async
    test('test updatePersonalData', () async {
      // TODO
    });

    // 
    //
    //Future validateEmail(String id, String token) async
    test('test validateEmail', () async {
      // TODO
    });

  });
}
