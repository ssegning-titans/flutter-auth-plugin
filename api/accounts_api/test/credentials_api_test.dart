//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:accounts_api/api.dart';
import 'package:test/test.dart';


/// tests for CredentialsApi
void main() {
  // final instance = CredentialsApi();

  group('tests for CredentialsApi', () {
    // 
    //
    //Future<CreateCredentialResponse> createCredential(String id, CredentialType type, CreateCredentialInput createCredentialInput) async
    test('test createCredential', () async {
      // TODO
    });

    // 
    //
    //Future<DisableCredentialResponse> disableCredential(String id, CredentialType type, Map<String, String> requestBody) async
    test('test disableCredential', () async {
      // TODO
    });

    // 
    //
    //Future<HasAccountPassword> hasCredential(String id, CredentialType type) async
    test('test hasCredential', () async {
      // TODO
    });

    // 
    //
    //Future<RecoverPasswordResponse> recoverPassword(String id, CredentialType type) async
    test('test recoverPassword', () async {
      // TODO
    });

    // 
    //
    //Future<ValidatePasswordResponse> validateCredential(String id, CredentialType type, ValidatePasswordInput validatePasswordInput) async
    test('test validateCredential', () async {
      // TODO
    });

    // 
    //
    //Future<ValidateRecoveryCodeResponse> validateRecoveryPassword(String id, CredentialType type, ValidateRecoveryCodeInput validateRecoveryCodeInput) async
    test('test validateRecoveryPassword', () async {
      // TODO
    });

    // 
    //
    //Future<ValidateRecoveryCodeResponse> validateRecoveryTokenPassword(String id, CredentialType type, ValidateRecoveryTokenInput validateRecoveryTokenInput) async
    test('test validateRecoveryTokenPassword', () async {
      // TODO
    });

  });
}
