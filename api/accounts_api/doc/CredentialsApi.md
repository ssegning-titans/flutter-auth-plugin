# accounts_api.api.CredentialsApi

## Load the API package
```dart
import 'package:accounts_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCredential**](CredentialsApi.md#createcredential) | **POST** /v1/auth/accounts/{id}/credentials/{type} | 
[**disableCredential**](CredentialsApi.md#disablecredential) | **PUT** /v1/auth/accounts/{id}/credentials/{type}/disable | 
[**hasCredential**](CredentialsApi.md#hascredential) | **GET** /v1/auth/accounts/{id}/credentials/{type} | 
[**recoverPassword**](CredentialsApi.md#recoverpassword) | **POST** /v1/auth/accounts/{id}/credentials/{type}/recover | 
[**validateCredential**](CredentialsApi.md#validatecredential) | **POST** /v1/auth/accounts/{id}/credentials/{type}/verify | 
[**validateRecoveryPassword**](CredentialsApi.md#validaterecoverypassword) | **POST** /v1/auth/accounts/{id}/credentials/{type}/validate | 
[**validateRecoveryTokenPassword**](CredentialsApi.md#validaterecoverytokenpassword) | **POST** /v1/auth/accounts/{id}/credentials/{type}/validate-token | 


# **createCredential**
> CreateCredentialResponse createCredential(id, type, createCredentialInput)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 
final createCredentialInput = CreateCredentialInput(); // CreateCredentialInput | 

try {
    final result = api_instance.createCredential(id, type, createCredentialInput);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->createCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **createCredentialInput** | [**CreateCredentialInput**](CreateCredentialInput.md)|  | 

### Return type

[**CreateCredentialResponse**](CreateCredentialResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **disableCredential**
> DisableCredentialResponse disableCredential(id, type, requestBody)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 
final requestBody = Map<String, String>(); // Map<String, String> | 

try {
    final result = api_instance.disableCredential(id, type, requestBody);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->disableCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **requestBody** | [**Map<String, String>**](String.md)|  | 

### Return type

[**DisableCredentialResponse**](DisableCredentialResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hasCredential**
> HasAccountPassword hasCredential(id, type)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 

try {
    final result = api_instance.hasCredential(id, type);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->hasCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 

### Return type

[**HasAccountPassword**](HasAccountPassword.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recoverPassword**
> RecoverPasswordResponse recoverPassword(id, type)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 

try {
    final result = api_instance.recoverPassword(id, type);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->recoverPassword: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 

### Return type

[**RecoverPasswordResponse**](RecoverPasswordResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateCredential**
> ValidatePasswordResponse validateCredential(id, type, validatePasswordInput)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 
final validatePasswordInput = ValidatePasswordInput(); // ValidatePasswordInput | 

try {
    final result = api_instance.validateCredential(id, type, validatePasswordInput);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->validateCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **validatePasswordInput** | [**ValidatePasswordInput**](ValidatePasswordInput.md)|  | 

### Return type

[**ValidatePasswordResponse**](ValidatePasswordResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateRecoveryPassword**
> ValidateRecoveryCodeResponse validateRecoveryPassword(id, type, validateRecoveryCodeInput)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 
final validateRecoveryCodeInput = ValidateRecoveryCodeInput(); // ValidateRecoveryCodeInput | 

try {
    final result = api_instance.validateRecoveryPassword(id, type, validateRecoveryCodeInput);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->validateRecoveryPassword: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **validateRecoveryCodeInput** | [**ValidateRecoveryCodeInput**](ValidateRecoveryCodeInput.md)|  | 

### Return type

[**ValidateRecoveryCodeResponse**](ValidateRecoveryCodeResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateRecoveryTokenPassword**
> ValidateRecoveryCodeResponse validateRecoveryTokenPassword(id, type, validateRecoveryTokenInput)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = CredentialsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final type = ; // CredentialType | 
final validateRecoveryTokenInput = ValidateRecoveryTokenInput(); // ValidateRecoveryTokenInput | 

try {
    final result = api_instance.validateRecoveryTokenPassword(id, type, validateRecoveryTokenInput);
    print(result);
} catch (e) {
    print('Exception when calling CredentialsApi->validateRecoveryTokenPassword: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **validateRecoveryTokenInput** | [**ValidateRecoveryTokenInput**](ValidateRecoveryTokenInput.md)|  | 

### Return type

[**ValidateRecoveryCodeResponse**](ValidateRecoveryCodeResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

