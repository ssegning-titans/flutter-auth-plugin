# accounts_api.api.AccountsApi

## Load the API package
```dart
import 'package:accounts_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countAccountsByParams**](AccountsApi.md#countaccountsbyparams) | **POST** /v1/auth/accounts/count/map | Get accounts by params
[**createAccount**](AccountsApi.md#createaccount) | **POST** /v1/auth/accounts | 
[**findAccountByAll**](AccountsApi.md#findaccountbyall) | **POST** /v1/auth/accounts/find/map | 
[**getAccountByIdentifier**](AccountsApi.md#getaccountbyidentifier) | **GET** /v1/auth/accounts/{id} | 
[**updatePersonalData**](AccountsApi.md#updatepersonaldata) | **PUT** /v1/auth/accounts/{id}/personal-data | 
[**validateEmail**](AccountsApi.md#validateemail) | **GET** /v1/auth/accounts/{id}/email/verify | 


# **countAccountsByParams**
> CountResponse countAccountsByParams(size, offset, requestBody)

Get accounts by params

Get `Accounts` by params. Empty `Map` means no search param, which also means to count all 

### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final size = 56; // int | 
final offset = 56; // int | 
final requestBody = Map<String, String>(); // Map<String, String> | 

try {
    final result = api_instance.countAccountsByParams(size, offset, requestBody);
    print(result);
} catch (e) {
    print('Exception when calling AccountsApi->countAccountsByParams: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  | 
 **offset** | **int**|  | 
 **requestBody** | [**Map<String, String>**](String.md)|  | 

### Return type

[**CountResponse**](CountResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createAccount**
> Account createAccount(createAccountInput)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final createAccountInput = CreateAccountInput(); // CreateAccountInput | 

try {
    final result = api_instance.createAccount(createAccountInput);
    print(result);
} catch (e) {
    print('Exception when calling AccountsApi->createAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createAccountInput** | [**CreateAccountInput**](CreateAccountInput.md)|  | 

### Return type

[**Account**](Account.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findAccountByAll**
> List<Account> findAccountByAll(size, offset, requestBody)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final size = 56; // int | 
final offset = 56; // int | 
final requestBody = Map<String, String>(); // Map<String, String> | 

try {
    final result = api_instance.findAccountByAll(size, offset, requestBody);
    print(result);
} catch (e) {
    print('Exception when calling AccountsApi->findAccountByAll: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **size** | **int**|  | 
 **offset** | **int**|  | 
 **requestBody** | [**Map<String, String>**](String.md)|  | 

### Return type

[**List<Account>**](Account.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAccountByIdentifier**
> Account getAccountByIdentifier(id)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final id = id_example; // String | 

try {
    final result = api_instance.getAccountByIdentifier(id);
    print(result);
} catch (e) {
    print('Exception when calling AccountsApi->getAccountByIdentifier: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Account**](Account.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updatePersonalData**
> updatePersonalData(id, changePersonalDataRequest)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final changePersonalDataRequest = ChangePersonalDataRequest(); // ChangePersonalDataRequest | 

try {
    api_instance.updatePersonalData(id, changePersonalDataRequest);
} catch (e) {
    print('Exception when calling AccountsApi->updatePersonalData: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **changePersonalDataRequest** | [**ChangePersonalDataRequest**](ChangePersonalDataRequest.md)|  | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateEmail**
> validateEmail(id, token)



### Example
```dart
import 'package:accounts_api/api.dart';
// TODO Configure HTTP basic authorization: basic
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('basic').password = 'YOUR_PASSWORD';

final api_instance = AccountsApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final token = token_example; // String | 

try {
    api_instance.validateEmail(id, token);
} catch (e) {
    print('Exception when calling AccountsApi->validateEmail: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **token** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

