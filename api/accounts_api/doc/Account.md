# accounts_api.model.Account

## Load the model package
```dart
import 'package:accounts_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **int** |  | 
**updatedAt** | **int** |  | 
**id** | **String** |  | 
**title** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**locale** | **String** |  | [optional] 
**avatarUrl** | **String** |  | [optional] 
**email** | **String** |  | 
**emailVerified** | **bool** |  | [optional] 
**gender** | [**AccountGender**](AccountGender.md) |  | [optional] 
**status** | [**AccountStatus**](AccountStatus.md) |  | 
**phoneNumber** | **String** |  | [optional] 
**phoneNumberVerified** | **bool** |  | [optional] 
**bio** | **String** |  | [optional] 
**accountType** | [**AccountType**](AccountType.md) |  | [optional] 
**addresses** | [**List<AccountAddress>**](AccountAddress.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


