# accounts_api.model.ChangePersonalDataRequest

## Load the model package
```dart
import 'package:accounts_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**bio** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**avatarUrl** | **String** |  | [optional] 
**phoneNumberValid** | **bool** |  | [optional] 
**locale** | **String** |  | [optional] 
**gender** | [**AccountGender**](AccountGender.md) |  | [optional] 
**accountType** | [**AccountType**](AccountType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


