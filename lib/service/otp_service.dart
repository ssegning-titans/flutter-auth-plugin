import 'package:otp/otp.dart';
import 'package:uuid/uuid.dart';

class OtpService {
  String? _secret;
  String? _issuer;
  Uuid _uuid = Uuid();

  int get _now {
    return DateTime.now().microsecondsSinceEpoch;
  }

  String get secret {
    final secret = _secret;
    if (secret == null) {
      throw new Exception(
          'No Secret is found!. Please first call #setSecret before using this.');
    }
    return secret;
  }

  String get issuer {
    final issuer = _issuer;
    if (issuer == null) {
      throw new Exception(
          'No Issuer is found!. Please first call #setSecret before using this.');
    }
    return issuer;
  }

  void setConfig({required String realm, OtpConfig? config}) {
    if (config != null) {
      _issuer = config.issuer;
      _secret = config.secret;
    } else {
      _issuer = 'https://accounts.ssegning.com/auth/realms/$realm';
      _secret = _generateSecret(realm);
    }
  }

  String _generateSecret(String realm) {
    _issuer = 'https://accounts.ssegning.com/auth/realms/$realm';
    return _uuid.v5(Uuid.NAMESPACE_URL, _issuer);
  }

  /// Get a screen show-able otp for login.
  String generateOtp() {
    return OTP.generateTOTPCodeString(secret, _now);
  }

  bool verify({required String code}) {
    return OTP.constantTimeVerification(
        OTP.generateTOTPCodeString(secret, _now), code);
  }

  /// This config is meant to be saved inside the mobile
  /// and never goes out.
  OtpConfig getConfig() {
    return OtpConfig(secret: secret, issuer: issuer);
  }

  void reset() {
    _secret = null;
  }
}

class OtpConfig {
  late String secret;
  late String issuer;

  OtpConfig({required this.secret, required this.issuer});

  OtpConfig.fromMap(Map<String, dynamic> map) {
    secret = map['secret'];
    issuer = map['issuer'];
  }

  factory OtpConfig.fromJson(Map<String, dynamic> json) {
    return OtpConfig.fromMap(json);
  }

  Map<String, String> toJson() => {
        'secret': secret,
        'issuer': issuer,
      };
}
