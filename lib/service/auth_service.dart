part of ssegning_auth;

class AuthService {
  static const String _TOKEN_KEY = 'token_key';
  static const String _OTP_KEY = 'otp_key';

  late AccountsApi _accountsApi;
  late CredentialsApi _credentialsApi;

  late AuthStorage _authStorage;
  late OtpService _otpService;

  late KeycloakOauth2Client _oauth2client;

  DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();

  final AuthOption _authOption;

  AuthModel? _authModel;

  AuthModel? get auth {
    return _authModel;
  }

  String get _accountId {
    if (_authModel?.decoded?.id != null) {
      return _authModel!.decoded!.id;
    }
    throw Exception('No logged in user');
  }

  AuthService._(this._authOption) {
    ApiClient apiClient = ApiClient(basePath: _authOption.apiUrl);
    _accountsApi = AccountsApi(apiClient);
    _credentialsApi = CredentialsApi(apiClient);

    _authStorage = AuthStorage();
    _otpService = OtpService();
    _authModel = AuthModel();

    _oauth2client = KeycloakOauth2Client(
      appScheme: _authOption.oAuth2Option.appScheme,
      realm: _authOption.oAuth2Option.realm,
    );
  }

  static AuthService? _authService;

  static AuthService build(AuthOption options) {
    if (_authService != null) {
      throw Exception('Cannot init _authService two times');
    }
    _authService = AuthService._(options);
    return _authService!;
  }

  static AuthService get authService => _authService!;

  Future<bool> _isApiReachable() async {
    var result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      try {
        final result = await InternetAddress.lookup(_authOption.apiUrl);
        return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
      } on SocketException catch (_) {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<void> init() async {
    final tokenResponse = await getToken();
    if (tokenResponse != null) {
      _authModel = _getAuthModel(tokenResponse);
      await _setupOtp();
      return;
    }
  }

  Future<TokenResponse?> _getSavedToken() async {
    var map = await _authStorage.getValue(_TOKEN_KEY);
    if (map == null) {
      return null;
    }
    return TokenResponse.fromJson(map);
  }

  Future<void> _setToken(TokenResponse tokenResponse) async {
    // Save token response with refresh token (important)
    await _authStorage.setValue(_TOKEN_KEY, tokenResponse);

    // Decode token and save it into variable
    _authModel = _getAuthModel(tokenResponse);
  }

  Future<void> signIn() async {
    if (!(await _isApiReachable())) {
      throw Exception('internet.not.reachable');
    }

    final scopes = _authOption.oAuth2Option.scopes ?? [];
    scopes.add('offline_access');
    scopes.add('email');
    scopes.add('user_data');

    // Login first
    var _tokenResponse = await _oauth2client.getTokenWithAuthCodeFlow(
      clientSecret: _authOption.oAuth2Option.secretId,
      clientId: _authOption.oAuth2Option.clientId,
      scopes: scopes,
      enableState: true,
      enablePKCE: true,
    );
    var tokenResponse = TokenResponse.fromParent(_tokenResponse);
    await _setToken(tokenResponse);

    await _setupOtp();
  }

  Future<OtpConfig?> _getOtpConfig() async {
    var map = await _authStorage.getValue(_OTP_KEY);
    if (map == null) {
      return null;
    }
    return OtpConfig.fromJson(map);
  }

  Future<void> _setupOtp() async {
    if (_authModel == null) {
      throw Exception(
          '#_authModel is not initialized. Please do so before going further');
    }

    // Get previous saved config
    OtpConfig? config = await _getOtpConfig();
    String realm = _authOption.oAuth2Option.realm;

    // Setup otp
    _otpService.setConfig(realm: realm, config: config);

    // Save config
    config = _otpService.getConfig();
    await _authStorage.setValue(_OTP_KEY, config);

    // Save the OTP into the backend so that server acknowledge this otp method.
    final credentialInput = CreateCredentialInput(
      challenge: config.secret,
      data: await _getDeviceData(),
    );
    await _credentialsApi.createCredential(
      _accountId,
      CredentialType.OTP,
      credentialInput,
    );
  }

  AuthModel? _getAuthModel(TokenResponse tokenResponse) {
    final accessToken = tokenResponse.accessToken;
    if (accessToken == null) {
      return null;
    }

    var decoded = Jwt.parseJwt(accessToken);
    return AuthModel(decodedMap: decoded);
  }

  Future<void> signOut() async {
    // Check if there's a token or not
    final tokenResponse = await _getSavedToken();
    if (tokenResponse == null) {
      return;
    }

    if (!(await _isApiReachable())) {
      throw Exception('internet.not.reachable');
    }

    // First revoke both tokens
    OAuth2Response auth2response =
        await _oauth2client.revokeToken(tokenResponse);
    if (auth2response.httpStatusCode >= 300) {
      throw Exception(auth2response);
    } else {
      await _authStorage.deleteValue(_TOKEN_KEY);
    }

    _authModel = AuthModel(decodedMap: null);

    // Then delete this OTP credential
    final response = await _credentialsApi.disableCredential(
      _accountId,
      CredentialType.OTP,
      await _getDeviceData(),
    );
    if (response?.state != true) {
      throw Exception('Cannot clear credentials');
    }

    await _getOtpConfig();
    _authStorage.deleteValue(_OTP_KEY);

    await _reset();
  }

  Future<Map<String, String>> _getDeviceData() async {
    Map<String, String> credentialData = HashMap();
    if (Platform.isAndroid) {
      credentialData['plate_form'] = 'android';

      AndroidDeviceInfo androidInfo = await _deviceInfo.androidInfo;
      if (androidInfo.id != null) credentialData['device_id'] = androidInfo.id!;
      if (androidInfo.model != null)
        credentialData['device_name'] = androidInfo.model!;
    } else if (Platform.isIOS) {
      credentialData['plate_form'] = 'ios';

      IosDeviceInfo iosDeviceInfo = await _deviceInfo.iosInfo;
      if (iosDeviceInfo.identifierForVendor != null)
        credentialData['device_id'] = iosDeviceInfo.identifierForVendor!;
      if (iosDeviceInfo.utsname.machine != null)
        credentialData['device_name'] = iosDeviceInfo.utsname.machine!;
    }

    return credentialData;
  }

  Future<void> _reset() async {
    await _authStorage.reset();
    _otpService.reset();
  }

  Future<void> updateData({
    String? email,
    String? firstName,
    String? lastName,
    String? bio,
    String? phoneNumber,
    String? avatarUrl,
    String? locale,
    AccountGender? gender,
  }) async {
    if (!(await _isApiReachable())) {
      throw Exception('internet.not.reachable');
    }

    ChangePersonalDataRequest request = ChangePersonalDataRequest();
    request.email = email;
    request.firstName = firstName;
    request.lastName = lastName;
    request.bio = bio;
    request.phoneNumber = phoneNumber;
    request.avatarUrl = avatarUrl;
    request.locale = locale;
    request.gender = gender;

    await _accountsApi.updatePersonalData(
      _accountId,
      request,
    );

    await getToken(forceRefresh: true);
  }

  Future<TokenResponse?> getToken({bool forceRefresh = false}) async {
    var tokenResponse = await _getSavedToken();
    if (tokenResponse == null) {
      return null;
    }

    if ((tokenResponse.isExpired() || forceRefresh) &&
        (await _isApiReachable())) {
      var _tokenResponse = await _oauth2client.refreshToken(
        tokenResponse.refreshToken!,
        clientSecret: _authOption.oAuth2Option.secretId,
        clientId: _authOption.oAuth2Option.clientId,
      );

      tokenResponse = TokenResponse.fromParent(_tokenResponse);

      await _setToken(tokenResponse);
    }

    return tokenResponse;
  }

  String getOtpCode() {
    return _otpService.generateOtp();
  }
}
