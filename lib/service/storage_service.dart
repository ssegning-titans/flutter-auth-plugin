part of ssegning_auth;

class AuthStorage {
  FlutterSecureStorage _storage;

  AuthStorage() : _storage = new FlutterSecureStorage();

  Future<dynamic> getValue(String key) async {
    final value = await _storage.read(key: key);
    if (value != null) {
      return jsonDecode(value);
    } else {
      return null;
    }
  }

  Future<void> setValue(String key, dynamic value) async {
    if (value != null) {
      String strVersion = jsonEncode(value);
      await _storage.write(key: key, value: strVersion);
    } else {
      this.deleteValue(key);
    }
  }

  Future<void> deleteValue(String key) async {
    await _storage.delete(key: key);
  }

  Future<void> reset() async {
    await _storage.deleteAll();
  }

}
