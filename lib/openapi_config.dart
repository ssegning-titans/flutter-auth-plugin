import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'accounts_api',
        pubAuthor: 'SSegning',
        pubHomepage: 'https://ssegning.com',
        pubVersion: '1.1.0',
        pubAuthorEmail: 'dev@ssegning.com',
    ),
    inputSpecFile: 'accounts-api.yml',
    generatorName: Generator.dart,
    outputDirectory: 'api/accounts_api',
    overwriteExistingFiles: true,
    alwaysRun: true,
    fetchDependencies: true,
)
class OpenAPiConfig extends OpenapiGeneratorConfig {}
