part of ssegning_auth;

class TokenResponse extends AccessTokenResponse {
  factory TokenResponse.fromJson(Map<String, dynamic> json) {
    AccessTokenResponse res = AccessTokenResponse.fromMap(json);
    return TokenResponse.fromParent(res);
  }

  factory TokenResponse.fromParent(AccessTokenResponse parent) {
    return TokenResponse.fromJson(parent.respMap);
  }

  TokenResponse();

  Map<String, dynamic> toJson() => toMap();
}
