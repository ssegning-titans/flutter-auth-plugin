import 'package:oauth2_client/oauth2_client.dart';

String _buildUrl(String realm, String action) =>
    'https://accounts.ssegning.com/auth/realms/$realm/protocol/openid-connect/$action';

class KeycloakOauth2Client extends OAuth2Client {
  KeycloakOauth2Client({
    required String appScheme,
    required String realm,
  }) : super(
          authorizeUrl: _buildUrl(realm, 'auth'),
          tokenUrl: _buildUrl(realm, 'token'),
          redirectUri: '$appScheme://oauth2redirect',
          customUriScheme: appScheme,
        );
}
