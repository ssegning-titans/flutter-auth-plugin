part of ssegning_auth;

class AuthOption {
  final String socketUrl;
  final String apiUrl;
  final OAuth2Option oAuth2Option;

  const AuthOption({
    required this.socketUrl,
    required this.apiUrl,
    required this.oAuth2Option,
  });
}

class OAuth2Option {
  final String realm;
  final List<String>? scopes;
  final String appScheme;
  final String clientId;
  final String? secretId;

  OAuth2Option({
    required this.realm,
    required this.appScheme,
    required this.clientId,
    this.secretId,
    this.scopes,
});
}
