part of ssegning_auth;

class AuthModel {
  TokenDecoded? decoded;

  AuthModel({Map<String, dynamic>? decodedMap})
      : this.decoded = decodedMap == null ? null : TokenDecoded(decodedMap);
}

class TokenDecoded {
  final Map<String, dynamic> _decodedMap;

  const TokenDecoded(this._decodedMap);

  String get email {
    return _decodedMap['email'];
  }

  String get name {
    return _decodedMap['name'];
  }

  String get sub {
    return _decodedMap['sub'];
  }

  /// Alias for #sub
  String get authId {
    return this.sub;
  }

  String get id {
    int idx = authId.lastIndexOf(':');
    return authId.substring(idx + 1);
  }

  String get emailVerified {
    return _decodedMap['email_verified'];
  }

  String? get preferredUsername {
    return _decodedMap['preferred_username'];
  }

  String? get locale {
    return _decodedMap['locale'];
  }

  String? get givenName {
    return _decodedMap['given_name'];
  }

  String? get familyName {
    return _decodedMap['family_name'];
  }

  String? get avatarUrl {
    return _decodedMap['picture'];
  }

  String? get gender {
    return _decodedMap['gender'];
  }

  String? get phoneNumber {
    return _decodedMap['phone_number'];
  }

  AddressDecoded? get address {
    Map<String, String>? address = _decodedMap['address'];
    if (address != null) {
      return AddressDecoded.fromMap(address);
    }
    return null;
  }
}

class AddressDecoded {
  String? formatted;
  String? streetAddress;
  String? locality;
  String? region;
  String? postalCode;
  String? country;

  AddressDecoded({
    this.formatted,
    this.streetAddress,
    this.locality,
    this.region,
    this.postalCode,
    this.country,
  });

  AddressDecoded.fromMap(Map<String, String> map) {
    this.formatted = map['formatted'];
    this.streetAddress = map['street_address'];
    this.locality = map['locality'];
    this.region = map['region'];
    this.postalCode = map['postal_code'];
    this.country = map['formatted'];
  }
}
